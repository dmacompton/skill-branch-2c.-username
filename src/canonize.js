export default function canonize(url) {
  const reg = new RegExp('@?(http?(s)?:)?(\/\/)?(www\.)?((telegram|vk|twitter)[^\/]*\/)?([a-zA-Z0-9\.]*)', 'i');
  const username = url.match(reg);
  console.log(username);
  return `@${username[7]}`;
}
