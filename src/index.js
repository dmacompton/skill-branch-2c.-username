import express from 'express';
import canonize from './canonize';

const app = express();

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.get('/', function (req, res) {
  let result;

  if ('username' in req.query) {
    let username = req.query.username;
    result = canonize(username);   
  } else {
    result = 'Invalid username';
  }

  res.send(result);
});

app.listen(3000, function () {
  console.log('Example app listening on port 3000!');
});
